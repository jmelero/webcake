// WEBCAKE

Esta es una guía rápida para saber contribuir en el proyecto.

1) Instalar git en vuestra computadora.

2) Mediante la terminal de git o bien por una terminal de consola, vas al directorio donde quieras tener el código fuente.

3) Ejecutais: git clone https://gitlab.com/jmelero/webcake.git

Una vez modifiques el código fuente:

1) git add .
2) git commit -m "Nombre del comentario"
3) git push origin master

origin: es la dirección por defecto, es decir https://gitlab.com/jmelero/webcake.git.
Puedes ver las direcciones mediante: git remote -v
master: es la rama a la que quieres enviar los cambios.

Cuando quieras añadir los cambios de los demás a tu código (repositorio local) escibres:

git pull origin master

En caso de conflictos os aparecerá un mensaje con ellos, entonces habrá que solucionarlos.